package id.andrisetyawan.quiztest.assets.model;

import com.google.gson.annotations.SerializedName;

public class BankJawaban {
    @SerializedName("id_bank_jawaban")
    private String idBankJawaban;
    @SerializedName("jawaban")
    private String jawaban;
    @SerializedName("bank_soal_id")
    private String bankSoalId;
    @SerializedName("ket_jawaban")
    private String ketJawaban;
}
