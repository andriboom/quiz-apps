package id.andrisetyawan.quiztest.assets.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResultStartQuiz {
    @SerializedName("teori")
    private List<JawabanUser> teori;
    @SerializedName("praktikum")
    private List<JawabanUser> praktikum;
    @SerializedName("user")
    private String user;

    public List<JawabanUser> getTeori() {
        return teori;
    }

    public void setTeori(List<JawabanUser> teori) {
        this.teori = teori;
    }

    public List<JawabanUser> getPraktikum() {
        return praktikum;
    }

    public void setPraktikum(List<JawabanUser> praktikum) {
        this.praktikum = praktikum;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
