package id.andrisetyawan.quiztest.assets.utils.retrofit.response;

import com.google.gson.annotations.SerializedName;

public class ResponseMessage {
    @SerializedName("status")
    private boolean status;
    @SerializedName("message")
    private String meesage;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMeesage() {
        return meesage;
    }

    public void setMeesage(String meesage) {
        this.meesage = meesage;
    }
}
