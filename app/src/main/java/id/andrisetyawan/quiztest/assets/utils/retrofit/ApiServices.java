package id.andrisetyawan.quiztest.assets.utils.retrofit;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import id.andrisetyawan.quiztest.assets.model.Materi;
import id.andrisetyawan.quiztest.assets.utils.retrofit.response.ResponseMessage;
import id.andrisetyawan.quiztest.assets.utils.retrofit.response.ResponseStartQuiz;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiServices {
    // get all materi
    @GET("materi")
    Call<List<Materi>> getAllMateri();

    //start quiz
    @FormUrlEncoded
    @POST("quiz/start")
    Call<ResponseStartQuiz> startQuiz(@Field("nama") String nama,@Field("ulangi") boolean ulangi);

    @FormUrlEncoded
    @POST("quiz/saveteori")
    Call<ResponseMessage> saveAnswareTeori(@Field("id") String id_user,@Field("soal") String id_soal,@Field("jawaban") String id_jawaban);

//    @FormUrlEncoded
//    @POST("quiz/hitungnilai")
//    Call<ResponseStartQuiz>
}
