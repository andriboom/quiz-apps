package id.andrisetyawan.quiztest.assets.model;

import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("id_user")
    private String idUser;
    @SerializedName("nama")
    private String nama;
    @SerializedName("start_quiz")
    private String startQuiz;

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getStartQuiz() {
        return startQuiz;
    }

    public void setStartQuiz(String startQuiz) {
        this.startQuiz = startQuiz;
    }
}
