package id.andrisetyawan.quiztest.assets.model;

import com.google.gson.annotations.SerializedName;

public class BankSoal {
    @SerializedName("id_bank_soal")
    private String idBankSoal;
    @SerializedName("butir_soal")
    private String butirSoal;
    @SerializedName("kategoriSoal")
    private String kategoriSoal;

    public BankSoal(String idBankSoal, String butirSoal, String kategoriSoal) {
        this.idBankSoal = idBankSoal;
        this.butirSoal = butirSoal;
        this.kategoriSoal = kategoriSoal;
    }
}
