package id.andrisetyawan.quiztest.assets.utils;

public class AppData {
    private static String IP = "quiz.andrisetyawan.id";
    public static final String URL = "http://"+IP+"/api/";
    public static final String URL_VIDEO = "http://"+IP+"/assets/materi/";

    public static final String TEORI_CODE = "1";
    public static final String PRAKTIKUM_CODE = "2";
}
