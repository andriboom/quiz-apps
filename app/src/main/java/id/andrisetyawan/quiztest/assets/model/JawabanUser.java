package id.andrisetyawan.quiztest.assets.model;

import com.google.gson.annotations.SerializedName;

public class JawabanUser {
    @SerializedName("id_jawaban_user")
    private String idJawabanUser;
    @SerializedName("user_id")
    private String userId;
    @SerializedName("bank_soal_id")
    private String bankSoalId;
    @SerializedName("bank_jawaban_id")
    private String bankJawabanId;
    @SerializedName("jawaban_teori")
    private String jawabanTeori;
    @SerializedName("jawaban_praktikum")
    private String jawabanPraktikum;

    public String getIdJawabanUser() {
        return idJawabanUser;
    }

    public void setIdJawabanUser(String idJawabanUser) {
        this.idJawabanUser = idJawabanUser;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBankSoalId() {
        return bankSoalId;
    }

    public void setBankSoalId(String bankSoalId) {
        this.bankSoalId = bankSoalId;
    }

    public String getBankJawabanId() {
        return bankJawabanId;
    }

    public void setBankJawabanId(String bankJawabanId) {
        this.bankJawabanId = bankJawabanId;
    }

    public String getJawabanTeori() {
        return jawabanTeori;
    }

    public void setJawabanTeori(String jawabanTeori) {
        this.jawabanTeori = jawabanTeori;
    }

    public String getJawabanPraktikum() {
        return jawabanPraktikum;
    }

    public void setJawabanPraktikum(String jawabanPraktikum) {
        this.jawabanPraktikum = jawabanPraktikum;
    }
}
