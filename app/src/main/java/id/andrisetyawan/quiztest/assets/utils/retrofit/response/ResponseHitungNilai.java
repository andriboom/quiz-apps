package id.andrisetyawan.quiztest.assets.utils.retrofit.response;

import com.google.gson.annotations.SerializedName;

public class ResponseHitungNilai {
    @SerializedName("status")
    private String status;
    @SerializedName("benar")
    private int totalBenar;
    @SerializedName("soal")
    private int idSoal;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTotalBenar() {
        return totalBenar;
    }

    public void setTotalBenar(int totalBenar) {
        this.totalBenar = totalBenar;
    }

    public int getIdSoal() {
        return idSoal;
    }

    public void setIdSoal(int idSoal) {
        this.idSoal = idSoal;
    }
}
