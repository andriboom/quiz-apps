package id.andrisetyawan.quiztest.assets.utils.retrofit;

import id.andrisetyawan.quiztest.assets.utils.AppData;

public class ApiUtils {
    private static String BASE_URL = AppData.URL;

    public static ApiServices getApiServices(){
        return RetrofitClient.getClient(BASE_URL).create(ApiServices.class);
    }
}
