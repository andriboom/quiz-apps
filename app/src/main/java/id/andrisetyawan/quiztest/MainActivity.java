package id.andrisetyawan.quiztest;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private Context context;
    @BindView(R.id.btn_materi)
    Button btnMateri;
    @BindView(R.id.btn_quiz)
    Button btnQuiz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        context = this;
    }

    @OnClick({R.id.btn_materi, R.id.btn_quiz})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_materi:
                startActivity(new Intent(context,MateriActivity.class));
                break;
            case R.id.btn_quiz:
                startActivity(new Intent(context,QuizActivity.class));
                break;
        }
    }
}
